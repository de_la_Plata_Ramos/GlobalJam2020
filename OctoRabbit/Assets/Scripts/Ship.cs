﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Ship : MonoBehaviour
{
    public float damagePerLostPart = 5.0f;
    public float healthRecoveredIfRepaired = 2.5f;
    public float healthUpdateInterval = 5.0f;

    public GameObject SparksSprite;

    private List<Part> parts;
    private Dictionary<Part, GameObject> sparks = new Dictionary<Part, GameObject>();
    private int numberOfParts;
    public float health = 100.0f;

    public HealthBar healthBar;

    public delegate void LifeChange();
    public event LifeChange OnLifeChange;
    public AudioSource[] audioSources;

    // Start is called before the first frame update
    void Awake()
    {
        audioSources = GetComponents<AudioSource>();
        parts = new List<Part>(GetComponentsInChildren<Part>());
        numberOfParts = parts.Count;

        transform.DOLocalMoveY(-1.8f, 3).SetLoops(-1, LoopType.Yoyo);
        transform.DOLocalMoveX(-3.5f, 2.5f).SetLoops(-1, LoopType.Yoyo);

        StartCoroutine("UpdateHealth");
    }

    // Update is called once per frame
    void Update()
    {

    }

    void DetachPart(int index)
    {
        if (index < parts.Count)
        {
            Part part = parts[index];

            // Detach part from ship so that it can move independly
            parts.Remove(part);

            // Notify the part that it's been detached
            part.OnDetachedFromShip();

            // Create sparks
            Vector3 sparkPosition = part.transform.position;
            GameObject spark = Instantiate(SparksSprite, sparkPosition, Quaternion.identity);
            spark.transform.parent = this.transform;
            sparks.Add(part, spark);
        }
    }

    public void DetachRandomPart()
    {
        DetachPart(Random.Range(0, parts.Count));
    }

    public void Attach(Part part)
    {
        part.transform.parent = this.transform;
        parts.Add(part);

        // Remove sparks
        GameObject spark;
        bool success = sparks.TryGetValue(part, out spark);

        if (success)
        {
            Destroy(spark.gameObject);
            sparks.Remove(part);
        }
    }

    private IEnumerator UpdateHealth()
    {
        while (health > 0.0f)
        {
            if (numberOfParts == parts.Count)
            {
                health += healthRecoveredIfRepaired;
                health = Mathf.Min(health, 100.0f);
            }
            else
            {
                health -= (numberOfParts - parts.Count) * damagePerLostPart;
                health = Mathf.Max(health, 0.0f);
                audioSources[0].Play();
                Camera.main.transform.DOShakePosition(0.5f, 0.2f);
            }

            if (OnLifeChange != null)
            {
                OnLifeChange();
            }

            healthBar.UpdateHealth(health);
            yield return new WaitForSeconds(healthUpdateInterval);
        }
    }
}
