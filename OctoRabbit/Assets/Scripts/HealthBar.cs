﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    private Image imageRenderer;

    void Start()
    {
        imageRenderer = GetComponent<Image>();
        imageRenderer.color = new Color(0.945f, 0.521f, 0.702f);
    }

    public void UpdateHealth(float health)
    {
        if (!imageRenderer)
        {
            imageRenderer = GetComponent<Image>();
        }
        float healthBarNumber = (health * 1920) / 100;
        transform.localPosition = new Vector3(healthBarNumber - 1920, transform.localPosition.y, 0);
        if (health >= 60)
        {
            imageRenderer.color = new Color(0.945f, 0.521f, 0.702f);
        }
        else if (health < 60 && health > 30)
        {
            imageRenderer.color = new Color(0.835f, 0.141f, 0.517f);
        }
        else
        {
            imageRenderer.color = new Color(0.564f, 0, 0.498f);
        }
    }
}
