﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SingletonObject : MonoBehaviour
{
    public Animator transition;
    public static SingletonObject Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public IEnumerator StartGame()
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(1);
        transition.SetTrigger("Reset");
        SceneManager.LoadScene(1);
    }

    public IEnumerator RestartGame()
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(1);
        transition.SetTrigger("Reset");
        SceneManager.LoadScene(0);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public IEnumerator Credits()
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(1);
        transition.SetTrigger("Reset");
        SceneManager.LoadScene(2);
    }
    
}