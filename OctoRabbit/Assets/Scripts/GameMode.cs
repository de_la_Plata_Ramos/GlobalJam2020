﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class GameMode : MonoBehaviour
{
    [SerializeField] private Ship ship;
    [SerializeField] private GameObject spaceJunk;

    [SerializeField] private float spawnJunkSpeed = 10;

    [SerializeField] private float maxShipBreakInterval = 10.0f;
    [SerializeField] private float minShipBreakInterval = 3.0f;
    [SerializeField] private float shipIntervalReductionPerSecond = 0.1f;
    private float currentShipBreakInterval;

    [SerializeField] private Transform[] spawnPositions;

    private bool isGameOver = false;
    public GameObject gameOverMenu;
    public GameObject gameOverText;
    public PlayerController playerController;
    public float timer;

    // Start is called before the first frame update
    void Start()
    {
        timer = 0;
        ship.OnLifeChange += CheckIfGameOver;
        gameOverMenu.SetActive(false);

        foreach (Transform junk in spawnPositions)
        {
            junk.position = new Vector3(junk.position.x, junk.position.y, 0.0f);
        }

        StartCoroutine("SpawnSpaceJunk");

        currentShipBreakInterval = maxShipBreakInterval;

        StartCoroutine("BreakShip");
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;


        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameOver();
        }
    }

    IEnumerator SpawnSpaceJunk()
    {
        while (!isGameOver)
        {
            if (spawnPositions.Length > 0)
            {
                int index = Random.Range(0, spawnPositions.Length);
                Instantiate(spaceJunk, spawnPositions[index].position, Quaternion.identity);
                yield return new WaitForSeconds(spawnJunkSpeed);
            }
        }
    }

    IEnumerator BreakShip()
    {
        // Esperar 1 segundo antes de empezar
        yield return new WaitForSeconds(1.0f);

        while (!isGameOver)
        {
            currentShipBreakInterval -= shipIntervalReductionPerSecond * currentShipBreakInterval;
            currentShipBreakInterval = Mathf.Max(minShipBreakInterval, currentShipBreakInterval);

            ship.DetachRandomPart();

            yield return new WaitForSeconds(currentShipBreakInterval);
        }
    }

    public void CheckIfGameOver()
    {
        if (ship.health <= 0.0f)
        {
            this.GameOver();
        }
    }

    private void GameOver()
    {
        isGameOver = true;
        ship.OnLifeChange += CheckIfGameOver;
        ship.audioSources[1].Play();
        AnimationShipGameOver(15);

        gameOverMenu.SetActive(true);
        gameOverText.GetComponent<Text>().text = Mathf.RoundToInt(timer).ToString();
        playerController.PauseInput();
    }

    private void AnimationShipGameOver(int time)
    {
        Sequence mySequence = DOTween.Sequence();
        mySequence.Append(ship.transform.DOMoveY(-15, time));
        mySequence.Join(ship.transform.DOShakeScale(0.5f, 0.1f).SetLoops(time * 2));
    }
}
