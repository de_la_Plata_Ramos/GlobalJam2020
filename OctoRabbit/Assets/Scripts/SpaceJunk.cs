﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SpaceJunk : MonoBehaviour, IInteractableObject
{
    public float releaseSpeed = 90.0f;
    public Sprite[] sprites;
    public SpriteRenderer spriteRenderer;

    private FlyingMovement flyingMovement;
    private bool fly = true;
    private bool justReleased = false;
    private Vector3 releaseDirection;
    private AudioSource[] audioSources;

    // Start is called before the first frame update
    void Start()
    {
        audioSources = GetComponents<AudioSource>();
        spriteRenderer.sprite = sprites[Random.Range(0, sprites.Length)];
        flyingMovement = GetComponentInChildren<FlyingMovement>();

        if (transform.position.x < 0)
            flyingMovement.flySpeed *= -1;
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void FixedUpdate()
    {
        if (fly)
            flyingMovement.Fly();

        if (justReleased)
            transform.position += releaseDirection * releaseSpeed * Time.deltaTime;

        // Check if we have to despawn
        if (fly || justReleased)
        {
            float distance = Vector3.Distance(Vector3.zero, transform.position);

            if (distance > Camera.main.orthographicSize * Camera.main.aspect * 2)
                Destroy(this.gameObject);
        }
    }

    bool IInteractableObject.Grabbed()
    {
        AnimationOnGrab(0.5f, 0.15f);
        bool canGrab = fly && !justReleased;
        fly = !canGrab;

        return canGrab;
    }

    void IInteractableObject.Released()
    {
        fly = true;
        justReleased = true;

        releaseDirection.x = Random.Range(-1.0f, 1.0f);
        releaseDirection.y = Random.Range(-1.0f, 1.0f);

        releaseDirection = releaseDirection.normalized;
        audioSources[0].Play();
    }

    void IInteractableObject.SetPosition(Vector3 position)
    {
        transform.position = position;
    }

    GameObject IInteractableObject.GetObject()
    {
        return this.gameObject;
    }

    public void AnimationOnGrab(float scaleTime, float strongness)
    {
        transform.DOShakeScale(scaleTime, strongness);
        audioSources[1].Play();
    }
}
