﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private List<Tentacle> tentacles = new List<Tentacle>();
    private Tentacle currentTentacle = null;

    private Vector2 oldMouseAxis;
    private Vector2 oldMousePosition;
    private bool isShaking = false;
    private float lastTime = 0.0f;
    private int shakingCount = 0;

    private bool acceptsInput = true;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (acceptsInput)
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                this.ActiveNewTenctacle(0);
            }

            if (Input.GetKeyDown(KeyCode.S))
            {
                this.ActiveNewTenctacle(1);
            }

            if (Input.GetKeyDown(KeyCode.D))
            {
                this.ActiveNewTenctacle(2);
            }

            if (Input.GetKeyDown(KeyCode.F))
            {
                this.ActiveNewTenctacle(3);
            }

            if (currentTentacle != null)
            {
                if (Input.GetKeyDown(KeyCode.Q))
                {
                    this.currentTentacle.Deactivate();
                }

                if (Input.GetMouseButtonDown(0))
                {
                    this.currentTentacle.GrabObject();
                }

                if (Input.GetMouseButtonUp(0))
                {
                    if (!this.currentTentacle.HasSpaceJunkGrabbed())
                        this.currentTentacle.ReleaseObject();
                }

                if (this.IsShakingMouse())
                {
                    this.currentTentacle.ReleaseObject();
                }

            }
        }
    }

    private void ActiveNewTenctacle(int tentacleIndex)
    {
        if (tentacleIndex < tentacles.Count)
        {
            if (currentTentacle != null)
            {
                if (!currentTentacle.HasSpaceJunkGrabbed())
                {
                    currentTentacle.ReleaseObject();
                }
                currentTentacle.Deactivate();
            }
            currentTentacle = tentacles[tentacleIndex];
            currentTentacle.Activate();
        }

    }

    private bool IsShakingMouse()
    {
        Vector2 mouseAxis = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        Vector2 mousePosition = Input.mousePosition;

        this.isShaking = (Mathf.Sign(mouseAxis.x) != Mathf.Sign(this.oldMouseAxis.x) || Mathf.Sign(mouseAxis.y) != Mathf.Sign(this.oldMouseAxis.y));
        this.isShaking = this.isShaking && (Vector2.Distance(mousePosition, oldMousePosition) >= 10.0f);
        this.oldMouseAxis = mouseAxis;

        if (this.isShaking)
        {
            if ((1.0f - lastTime) <= 0)
            {
                shakingCount++;
                if (shakingCount > 5)
                {
                    shakingCount = 0;
                    lastTime = 0;
                    return true;
                }
            }
            else
            {
                lastTime += Time.deltaTime;
            }
        }

        return false;
    }

    public void PauseInput()
    {
        acceptsInput = false;
        if (currentTentacle != null)
        {
            currentTentacle.Deactivate();
        }
    }
}
