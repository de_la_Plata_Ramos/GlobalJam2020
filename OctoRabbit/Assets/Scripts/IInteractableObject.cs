﻿using UnityEngine;

public interface IInteractableObject
{
    bool Grabbed();
    void Released();
    void SetPosition(Vector3 position);

    GameObject GetObject();
}
