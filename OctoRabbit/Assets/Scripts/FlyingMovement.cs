﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingMovement : MonoBehaviour
{
    public float flySpeed = 0.5f;

    private float randomOffset;
    public Rigidbody2D rigidbody;

    // Start is called before the first frame update
    void Start()
    {
        randomOffset = Random.Range(0.0f, 3.0f);
        rigidbody = GetComponentInChildren<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Fly()
    {
        Vector2 movement = Vector2.left * flySpeed * Time.deltaTime;

        // Añadir oscilación vertical
        // Sumamos explosionPosition.x porque es un número aleatorio distinto para cada parte
        // Así no todas las partes oscilan a la vez
        movement.y += Mathf.Sin(Time.time + randomOffset) * 0.006f;

        rigidbody.MovePosition(rigidbody.position + movement);
    }
}
