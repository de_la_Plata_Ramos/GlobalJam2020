﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButton : MonoBehaviour
{
	[SerializeField] MenuButtonController menuButtonController;
	[SerializeField] Animator animator;
	[SerializeField] AnimatorFunctions animatorFunctions;
	[SerializeField] int thisIndex;
    [SerializeField] int selectFunction;
    public SingletonObject singletonObject;
    private bool pressedAlready = false;

    void Start()
    {
        singletonObject = GameObject.FindGameObjectWithTag("Singleton").GetComponent<SingletonObject>();
    }

    // Update is called once per frame
    void Update()
    {


		if(menuButtonController.index == thisIndex || Input.mousePosition == this.transform.position)
		{

            animator.SetBool ("selected", true);
			if((Input.GetAxis ("Submit") == 1 || Input.GetMouseButtonDown(0) )&& !pressedAlready)
            {
                pressedAlready = true;
                animator.SetBool ("pressed", true);
				animatorFunctions.press = true;
                if (selectFunction == 1)
                {
                    StartCoroutine(singletonObject.StartGame());
                }
                else if (selectFunction == 2)
                {
                    StartCoroutine(singletonObject.RestartGame());
                }
                else if (selectFunction == 3)
                {
                    singletonObject.ExitGame();
                }
                else if (selectFunction == 4)
                {
                    StartCoroutine(singletonObject.Credits());
                }
            }
			else if (animator.GetBool ("pressed")){
				animator.SetBool ("pressed", false);
				animatorFunctions.playOnce = true;
				animatorFunctions.press = false;
			}
		}else{
			animator.SetBool ("selected", false);
		}
    }

}
