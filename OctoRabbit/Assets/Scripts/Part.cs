﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Part : MonoBehaviour, IInteractableObject
{
    public float explosionTime = 0.25f;
    public float explosionMaxAngleUp = 95.0f;
    public float explosionMaxAngleDown = -45.0f;
    public float screenMarginMin = 1.8f;
    public float screenMarginMax = 2.5f;
    public float maxDistanceToCorrectPosition = 4.0f;
    public GameObject spritePart;

    public float beforeDetachAnimationTime = 1f;

    private Vector3 initialPosition;
    private Vector3 initialPositionLocal;
    private Vector3 explosionPosition;
    private Vector3 explosionCurrentSpeed;
    private Rigidbody2D rigidbody;
    private FlyingMovement flyingMovement;
    private AudioSource[] audioSources;
    private Material mat;
    private Tween grabAnimation;

    public Ship ship;
    [SerializeField] private State state = State.attached;

    // Start is called before the first frame update
    void Start()
    {
        mat = spritePart.GetComponent<SpriteRenderer>().material;
        mat.SetFloat("_Thickness", 0f);
        audioSources = GetComponents<AudioSource>();

        initialPosition = transform.position;
        initialPositionLocal = transform.localPosition;

        rigidbody = GetComponent<Rigidbody2D>();
        flyingMovement = GetComponentInChildren<FlyingMovement>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {
        switch (state)
        {
            case State.explosion:
                transform.position = Vector3.SmoothDamp(transform.position, explosionPosition, ref explosionCurrentSpeed, explosionTime);

                if (Vector3.Distance(transform.position, explosionPosition) < 0.2f)
                    state = State.flying;

                break;

            case State.flying:
                flyingMovement.Fly();

                break;

            default:
                break;
        }
    }

    // Called when the part is detached from the ship
    public void OnDetachedFromShip()
    {
        StartCoroutine("DetachFromShip");
    }

    private IEnumerator DetachFromShip()
    {
        AnimationBeforeDetach(beforeDetachAnimationTime);
        yield return new WaitForSeconds(1);

        transform.parent = null;
        StartFlyToScreenEdge();
    }

    private void StartFlyToScreenEdge()
    {
        state = State.explosion;

        // Select random position to fly to
        Camera camera = Camera.main;
        float angle = Mathf.Deg2Rad * Random.Range(explosionMaxAngleDown, explosionMaxAngleUp);

        explosionPosition.x = Mathf.Cos(angle);
        explosionPosition.y = Mathf.Sin(angle);

        explosionPosition *= camera.orthographicSize * camera.aspect;

        float screenMargin = Random.Range(screenMarginMin, screenMarginMax);

        float maxX = camera.orthographicSize * camera.aspect - screenMargin;
        float maxY = camera.orthographicSize - screenMargin;

        explosionPosition.x = Mathf.Clamp(explosionPosition.x, -maxX, maxX);
        explosionPosition.y = Mathf.Clamp(explosionPosition.y, -maxY, maxY);

        mat.SetFloat("_Thickness", 50f);
        AnimationAfterDetach(explosionTime * 2);
    }

    bool IInteractableObject.Grabbed()
    {
        if (state == State.flying || state == State.explosion)
        {
            state = State.grabbed;
            audioSources[1].Play();
            AnimationOnGrab(0.5f, 0.15f);
            return true;
        }

        return false;
    }

    void IInteractableObject.Released()
    {
        if (Vector3.Distance(transform.position, ship.transform.position + initialPositionLocal) < maxDistanceToCorrectPosition)
        {
            ship.Attach(this);

            transform.localPosition = initialPositionLocal;
            transform.rotation = Quaternion.identity;

            state = State.attached;
            audioSources[0].Play();
            AnimationOnGrab(0.5f, 0.15f);
            mat.SetFloat("_Thickness", 0f);
        }
        else
        {
            state = State.flying;
        }
    }

    void IInteractableObject.SetPosition(Vector3 position)
    {
        transform.position = position;
    }

    GameObject IInteractableObject.GetObject()
    {
        return this.gameObject;
    }

    private enum State
    {
        attached,
        explosion,
        flying,
        grabbed
    }

    public void AnimationBeforeDetach(float shakeTime)
    {
        transform.DOShakeScale(shakeTime, 0.15f);
    }

    public void AnimationAfterDetach(float rotationTime)
    {
        audioSources[2].Play();
        transform.DORotate(new Vector3(0, 0, Random.Range(600, 1200)), rotationTime, RotateMode.FastBeyond360);
    }

    public void AnimationOnGrab(float scaleTime, float strongness)
    {
        if (grabAnimation != null)
        {
            grabAnimation.Rewind();
            grabAnimation.Kill();
        }

        grabAnimation = transform.DOShakeScale(scaleTime, strongness);
    }
}
