﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Tentacle : MonoBehaviour
{
    [SerializeField] private float maxRange = 10;
    [SerializeField] private float apertureAngle = 30.0f;
    [SerializeField] private Rigidbody2D endBone;

    private Vector2 initialPosition;
    private Vector2 initialDirection;
    private Vector2 coneOrigin;
    [SerializeField] float coneRange = 5;

    private bool isActive = false;

    private IInteractableObject grabObject = null;

    public GameObject repairMessage;
    public GameObject shakeMessage;

    private Sequence mySequence;

    void Awake()
    {
        initialPosition = this.gameObject.transform.GetChild(0).transform.position;
        initialDirection = (endBone.position - initialPosition).normalized;
        coneOrigin = initialPosition - (initialDirection * coneRange);
    }

    // Start is called before the first frame update
    void Start()
    {
        mySequence = DOTween.Sequence();
    }

    public bool HasSpaceJunkGrabbed()
    {
        if (grabObject != null)
        {
            if (grabObject.GetObject().GetComponent<SpaceJunk>() != null)
            {
                return true;
            }
        }
        return false;
    }

    public void Activate()
    {
        isActive = true;
        endBone.bodyType = RigidbodyType2D.Kinematic;
        //AnimationSelectedTentacle();

    }

    public void Deactivate()
    {
        isActive = false;
        endBone.bodyType = RigidbodyType2D.Dynamic;

    }

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            MoveTentacle();

            GrabJunk();
        }

        if (grabObject != null)
        {
            grabObject.SetPosition(endBone.position);
        }
    }

    private void MoveTentacle()
    {
        Vector2 newPosition = Vector2.zero;
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        float mouseDistance = Vector2.Distance(initialPosition, mousePosition);
        float range = maxRange;

        if (mouseDistance > range)
        {
            newPosition = (mousePosition - initialPosition).normalized;
            newPosition *= range;
            newPosition += initialPosition;
        }
        else
        {
            newPosition = mousePosition;
        }

        float angle = Vector2.SignedAngle(newPosition - initialPosition, initialDirection);
        if (Mathf.Abs(angle) >= apertureAngle)
        {
            float absoluteAngle = Vector2.SignedAngle(Vector2.right, initialDirection);
            if (absoluteAngle < 0.0f)
            {
                absoluteAngle = 360.0f + absoluteAngle;
            }
            absoluteAngle -= (apertureAngle * angle / Mathf.Abs(angle));

            float x = Mathf.Cos(absoluteAngle * Mathf.Deg2Rad);
            float y = Mathf.Sin(absoluteAngle * Mathf.Deg2Rad);
            newPosition = new Vector2(x, y).normalized;
            //range = (Mathf.Abs(angle) > 90) ? 1.0f : range;

            Vector3 projected = Vector3.Project(mousePosition - initialPosition, newPosition);
            float projectedMagnitude = projected.magnitude;

            if (range < 90.0f && Mathf.Abs(Vector3.Angle(newPosition, projected) - 180.0f) < 1.0f)
            {
                range = 0.0f;
            }

            newPosition *= Mathf.Min(projectedMagnitude, range);
            newPosition += initialPosition;
        }

        endBone.MovePosition(newPosition);

    }

    public void GrabJunk()
    {
        if (grabObject == null)
        {
            RaycastHit2D hit = Physics2D.Raycast(endBone.position, Vector3.back);
            if (hit.collider != null)
            {
                IInteractableObject junk = hit.rigidbody.gameObject.GetComponent<SpaceJunk>();
                if (junk != null)
                {
                    if (junk.Grabbed())
                    {
                        if (shakeMessage.transform.position.x < -9)
                        {
                            mySequence.Kill();
                            mySequence = DOTween.Sequence();
                            mySequence.Append(shakeMessage.transform.DOMoveX(-8, 1f).SetEase(Ease.OutElastic));
                            mySequence.Append(shakeMessage.transform.DOShakeScale(0.5f, 0.25f).SetLoops(-1, LoopType.Yoyo));
                        }
                        grabObject = junk;
                    }
                }
            }
        }
    }

    public void GrabObject()
    {
        if (grabObject == null)
        {
            RaycastHit2D hit = Physics2D.Raycast(endBone.position, Vector3.back);
            if (hit.collider != null)
            {
                IInteractableObject part = hit.rigidbody.gameObject.GetComponent<IInteractableObject>();
                if (part != null)
                {
                    if (part.Grabbed())
                    {
                        if (repairMessage.transform.position.x < -9)
                        {
                            mySequence.Kill();
                            mySequence = DOTween.Sequence();
                            mySequence.Append(repairMessage.transform.DOMoveX(-8, 1f).SetEase(Ease.OutElastic));
                            mySequence.Append(repairMessage.transform.DOShakeScale(0.8f, 0.25f).SetLoops(-1, LoopType.Yoyo));
                        }
                        grabObject = part;
                    }
                }
            }
        }
    }

    public void ReleaseObject()
    {
        if (shakeMessage.transform.position.x > -9 || repairMessage.transform.position.x > -9)
        {
            mySequence.Kill();
            mySequence = DOTween.Sequence();
            shakeMessage.transform.DOMoveX(-12, 1f);
            repairMessage.transform.DOMoveX(-12, 1f);
        }
        if (grabObject != null)
        {
            grabObject.Released();
            grabObject = null;
        }
    }

    public void AnimationSelectedTentacle(float force = 0.05f, float time = 0.5f)
    {
        transform.DOPunchScale(new Vector3(force, force), time);
    }
}
