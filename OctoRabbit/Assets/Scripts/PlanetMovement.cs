﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlanetMovement : MonoBehaviour
{

    void Start()
    {
        Invoke("AnimationInTime", Random.Range(0.2f, 2f));
    }

    void AnimationInTime()
    {
        transform.DOLocalMoveY(transform.position.y + Random.Range(0.2f, 0.5f), 3).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutBack);
    }
    
}
