﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorFunctions : MonoBehaviour
{
	[SerializeField] AudioClip soundSwitch;
	[SerializeField] AudioClip soundClick;
	[SerializeField] MenuButtonController menuButtonController;
	public bool playOnce;
	public bool press;

	void PlaySound(){
		if (!playOnce)
		{
			if (press)
			{
				menuButtonController.audioSource.PlayOneShot(soundClick);
			}
			else {
				menuButtonController.audioSource.PlayOneShot(soundSwitch);
			}
			
		}else{
			playOnce = false;
		}

	}

}	
